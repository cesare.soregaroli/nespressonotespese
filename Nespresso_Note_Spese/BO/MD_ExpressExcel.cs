﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Spreadsheet;
using DevExpress.XtraPrinting;
using DevExpress.XtraSpreadsheet.Services;
using DevExpress.XtraSpreadsheet.Services.Implementation;
using System;
using DevExpress.Spreadsheet.Export;

namespace MD.DevExpressMdg
{
    public class ExpressExcelMdg : System.IDisposable
    {
        bool disposed = false;
        DevExpress.Spreadsheet.Workbook _server = null;

        public ExpressExcelMdg()
        {
            _server = new Workbook();
        }

        public void CreateDocument(System.String _path)
        {
            try
            {
                _server.CreateNewDocument();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void loadDocument(System.String _path)
        {
            try
            {
                _server.LoadDocument(_path, DocumentFormat.OpenXml);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void loadDocument(System.String _path, DocumentFormat _typeFormat)
        {
            try
            {
                _server.Options.Import.Csv.ColumnTypes[0] = DevExpress.XtraSpreadsheet.Import.ColumnDataType.Text;
                _server.LoadDocument(_path, _typeFormat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddSheetToDocument(System.String _nameSheet)
        {
            try
            {
                _server.Worksheets.Add().Name = _nameSheet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RenameSheet(String nomeOld, String nomeNew)
        {
            try
            {
                List<Worksheet> _worksheet = _server.Worksheets.ToList();
                foreach (Worksheet _tmpworksheet in _worksheet)
                {
                    if (_tmpworksheet.Name == nomeOld)
                    {
                        _tmpworksheet.Name = nomeNew;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RenameSheet(int sheetPos, String nomeNew)
        {
            try
            {
                List<Worksheet> _worksheet = _server.Worksheets.ToList();
                foreach (Worksheet _tmpworksheet in _worksheet)
                {
                    if (_tmpworksheet.Index == sheetPos)
                    {
                        _tmpworksheet.Name = nomeNew;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddTextInCell(System.String _nameSheet, System.String _value)
        {
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                worksheet.Cells["A1"].SetValueFromText(_value);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public void AddTextInCell(System.String _nameSheet, System.Data.DataTable _valuedt ,Boolean _addRowHeader)
        {
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                worksheet.Import(_valuedt, _addRowHeader, 0, 0);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddTextInCell(System.String _nameSheet, System.Data.DataTable _valuedt, System.String _colonna, System.String _riga)
        {
            Int32 _indiceRiga = 0;
            Int32 _indiceColonna = 0;
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                _indiceRiga = worksheet.Rows[_riga].Index;
                _indiceColonna = worksheet.Columns[_colonna].Index;
                worksheet.Import(_valuedt, true, _indiceRiga, _indiceColonna);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddTextInCell(System.String _nameSheet, System.String _value, System.String _colonna, System.String _riga)
        {
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                worksheet.Cells[$"{_colonna}{_riga}"].SetValueFromText(_value);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ChangePivotRange(System.String _nameSheetPivot, System.String _nameSheetSource, System.String _namePivotTable, Range _range)
        {

            try
            {

                Worksheet sourceWorksheet = _server.Worksheets[_nameSheetSource];

                Worksheet worksheet = _server.Worksheets[_nameSheetPivot];

                // Access the pivot table by its name in the collection. 
                PivotTable pivotTable = worksheet.PivotTables[_namePivotTable];

                pivotTable.ChangeDataSource(_range);

                pivotTable.Cache.Refresh();

            }
            catch (Exception)
            {
                throw;
            }
        }


        public Range Range_UsedSheet(System.String _nameSheet)
        {
            Range _Range;
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                _Range = worksheet.GetUsedRange();
            }
            catch (Exception)
            {
                throw;
            }
            return _Range;
        }

        public Range Range_UsedSheet(System.Int16 _idSheet)
        {
            Range _Range;
            try
            {
                Worksheet worksheet = _server.Worksheets[_idSheet];
                _Range = worksheet.GetUsedRange();
            }
            catch (Exception)
            {
                throw;
            }
            return _Range;
        }


        public void Replicate_Rule(System.String _nameSheet, System.String _columnToCopy, Int32 _RowToCopy, Int32 _RowMaxCopy)
        {
            System.String _formula;
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                _formula = worksheet.Columns[_columnToCopy][_RowToCopy].Formula;
                worksheet.Range[$"{_columnToCopy}2:{_columnToCopy}{_RowMaxCopy}"].Formula = _formula;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Filte_Add(System.String _nameSheet, Range _range)
        {
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                worksheet.AutoFilter.Apply(_range);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<System.String> ExportSheetName()
        {
            List<System.String> _ListnameSheet = new List<string> { };
            try
            {
                List<Worksheet> _worksheet = _server.Worksheets.ToList();
                foreach (Worksheet _tmpworksheet in _worksheet)
                {
                    _ListnameSheet.Add(_tmpworksheet.Name);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _ListnameSheet;
        }

        public System.String ExportValueCell(System.String _nameSheet, System.String _range)
        {
            System.String _valueCell = string.Empty;
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                _valueCell = worksheet.Range[_range].Value.TextValue.ToString();
            }
            catch (Exception)
            {
                throw;
            }
            return _valueCell;
        }

        public void loadDocument(System.String _path, DocumentFormat _typeFormat, char _delimiter)
        {
            try
            {
                _server.Options.Import.Csv.ColumnTypes[0] = DevExpress.XtraSpreadsheet.Import.ColumnDataType.Text;
                _server.Options.Import.Csv.Delimiter = _delimiter;
                _server.LoadDocument(_path, _typeFormat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public System.Data.DataTable ExportDataTable(System.String _idSheet)
        {
            System.Data.DataTable _dtbSource = null;
            try
            {
                Worksheet worksheet = _server.Worksheets[_idSheet];
                Range _range = Range_UsedSheet(_idSheet);
                _dtbSource = worksheet.CreateDataTable(_range, true);
                
                //exporter.Options.ConvertEmptyCells = true;
                //exporter.Options.DefaultCellValueToColumnTypeConverter.SkipErrorValues = false;
                //exporter.Export();

                for (int col = 0; col < _range.ColumnCount; col++)
                {
                    CellValueType cellType = _range[0, col].Value.Type;
                    for (int r = 1; r < _range.RowCount; r++)
                    {
                        if (cellType != _range[r, col].Value.Type)
                        {
                            _dtbSource.Columns[col].DataType = typeof(string);
                            break;
                        }
                    }
                }

                DataTableExporter exporter = worksheet.CreateDataTableExporter(_range, _dtbSource, true);

                exporter.CellValueConversionError += exporter_CellValueConversionError;

                //// Specify a custom converter for the "As Of" column.
                //DateTimeToStringConverter toDateStringConverter = new DateTimeToStringConverter();
                //exporter.Options.CustomConverters.Add("As Of", toDateStringConverter);
                //// Set the export value for empty cell.
                //toDateStringConverter.EmptyCellValue = "N/A";
                //// Specify that empty cells and cells with errors should be processed.
                //exporter.Options.ConvertEmptyCells = true;
                //exporter.Options.DefaultCellValueToColumnTypeConverter.SkipErrorValues = false;

                exporter.Export();
            }
            catch (Exception)
            {
                throw;
            }
            return _dtbSource;
        }

        public System.Data.DataTable ExportDataTable(System.Int16 _idSheet)
        {
            System.Data.DataTable _dtbSource = null;
            try
            {
                Worksheet worksheet = _server.Worksheets[_idSheet];
                Range _range = Range_UsedSheet(_idSheet);
                _dtbSource = worksheet.CreateDataTable(_range, true);
                

                for (int col = 0; col < _range.ColumnCount; col++)
                {
                    CellValueType cellType = _range[0, col].Value.Type;
                    for (int r = 1; r < _range.RowCount; r++)
                    {
                        if (cellType != _range[r, col].Value.Type)
                        {
                            _dtbSource.Columns[col].DataType = typeof(string);
                            break;
                        }
                    }
                }

                DataTableExporter exporter = worksheet.CreateDataTableExporter(_range, _dtbSource, true);

                exporter.CellValueConversionError += exporter_CellValueConversionError;

                exporter.Export();

            }
            catch (Exception)
            {
                throw;
            }
            return _dtbSource;
        }

        public System.Data.DataTable ExportDataTableRemoveRow(System.Int16 _idSheet,System.Int16 _removeRow)
        {
            System.Data.DataTable _dtbSource = null;
            try
            {
                Worksheet worksheet = _server.Worksheets[_idSheet];
                worksheet.Rows.Remove(0, _removeRow);
                Range _range = Range_UsedSheet(_idSheet);
                
                _dtbSource = worksheet.CreateDataTable(_range, true);


                for (int col = 0; col < _range.ColumnCount; col++)
                {
                    CellValueType cellType = _range[0, col].Value.Type;
                    for (int r = 1; r < _range.RowCount; r++)
                    {
                        if (cellType != _range[r, col].Value.Type)
                        {
                            _dtbSource.Columns[col].DataType = typeof(string);
                            break;
                        }
                    }
                }

                DataTableExporter exporter = worksheet.CreateDataTableExporter(_range, _dtbSource, true);

                exporter.CellValueConversionError += exporter_CellValueConversionError;

                exporter.Export();

            }
            catch (Exception)
            {
                throw;
            }
            return _dtbSource;
        }

        public void SaveDocument(System.String _path)
        {
            try
            {
                _server.SaveDocument(_path, DocumentFormat.Xlsx);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SaveDocumentCSV(System.String _path,System.Char _separator)
        {
            try
            {
                _server.Options.Export.Csv.ValueSeparator = _separator;
                _server.SaveDocument(_path, DocumentFormat.Csv);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ExportPDFDocument(System.String _namePdf)
        {
            try
            {
                _server.ExportToPdf(_namePdf);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public void ExportPDFSheet(System.String _namePdf)
        {
            Workbook _servertmp = null;
            try
            {
                foreach (Worksheet _worksheet in _server.Worksheets)
                {
                    _servertmp = new Workbook();
                    _servertmp.Worksheets.Add(_worksheet.Name);
                    _servertmp.Worksheets[_worksheet.Name].CopyFrom(_worksheet);
                    _servertmp.ExportToPdf(_namePdf.Replace(".pdf", string.Concat(System.DateTime.Now.Millisecond.ToString(), ".pdf")));
                    _servertmp.Dispose();
                }
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
            }
            disposed = true;
        }

        void exporter_CellValueConversionError(object sender, CellValueConversionErrorEventArgs e)
        {
            e.DataTableValue = null;
            e.Action = DataTableExporterAction.Continue;
        }
    }

    class DateTimeToStringConverter : ICellValueToColumnTypeConverter
    {
        public bool SkipErrorValues { get; set; }
        public CellValue EmptyCellValue { get; set; }

        public ConversionResult Convert(Cell readOnlyCell, CellValue cellValue, Type dataColumnType, out object result)
        {
            result = DBNull.Value;
            ConversionResult converted = ConversionResult.Success;
            if (cellValue.IsEmpty)
            {
                result = EmptyCellValue;
                return converted;
            }
            if (cellValue.IsError)
            {
                // You can return an error, subsequently the exporter throws an exception if the CellValueConversionError event is unhandled.
                //return SkipErrorValues ? ConversionResult.Success : ConversionResult.Error;
                result = "N/A";
                return ConversionResult.Success;
            }
            result = String.Format("{0:MMMM-yyyy}", cellValue.DateTimeValue);
            return converted;
        }
    }

}
