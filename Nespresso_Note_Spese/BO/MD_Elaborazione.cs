﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MD.Errore;
using MD.Configuration;
using System.Globalization;
using System.IO;
using NESPRESSO_NOTESPESE.XMLConfigurazione;
using System.Data;
using MD.ExcelMDG;
using NLog.Fluent;

namespace MD.Elaborazione
{
    class MD_Elaborazione : MD_Errori
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private List<MD_NOTA_SPESE_ERRORI> _List_MD_NOTA_SPESE_ERRORI = new List<MD_NOTA_SPESE_ERRORI> { };
        private List<MD_NOTA_SPESE> _List_MD_NOTA_SPESE = new List<MD_NOTA_SPESE> { };
        public void ElaboraProcedura(NESPRESSO_Project _oNespressoProject)
        {
            DataTable _dt_CSV = null;
            DataTable _dt_XSLX = null;
            MD_Excel _oMD_Excel = null;

            try
            {                
                DirectoryInfo _workDir = new DirectoryInfo(_oNespressoProject.NESPRESSO_Input_WORK);
                if (_workDir.GetFiles().Length == 0) { Log.Info($"Nessun File presente nella cartella di lavoro: {_oNespressoProject.NESPRESSO_Input_WORK}"); }
                else
                {
                    FileInfo _oCSV = _workDir.GetFiles().Where(x => x.Extension.ToUpper().Equals(".CSV")).FirstOrDefault();
                    FileInfo _oXls = _workDir.GetFiles().Where(x => x.Extension.ToUpper().Equals(".XLS")).FirstOrDefault();

                    if (_oCSV == null) { throw new Exception($"CSV non trovato nella cartella di lavoro {_oNespressoProject.NESPRESSO_Input_WORK}"); }
                    if (_oXls == null) { throw new Exception($"CSV non trovato nella cartella di lavoro {_oNespressoProject.NESPRESSO_Input_WORK}"); }
                    _oMD_Excel = new MD_Excel();

                    //Estraggo i dati dal CSV
                    _dt_CSV = _oMD_Excel.load_CSV(_oCSV.FullName, 0, ',');
                    if (_dt_CSV == null || _dt_CSV.Rows.Count == 0) { throw new Exception($"Dati del CSV non Caricati correttamente {_oCSV.FullName}"); }

                    //Estraggo i dati dall'Excel
                    _dt_XSLX = _oMD_Excel.load_XSLS(_oXls.FullName, 0, MD_Configuration.TOOL_ROW_XSL_DELETE);
                    if (_dt_XSLX == null || _dt_XSLX.Rows.Count == 0) { throw new Exception($"Dati del XLS non Caricati correttamente {_oXls.FullName}"); }

                    log.Info(" ==> INIZIO Prodcedura_1");
                    //AZIONE 1 (Giustificativi)
                    procedura_1(_dt_CSV, _dt_XSLX, _oNespressoProject);
                    log.Info(" ==> FINE Prodcedura_1");

                    log.Info(" ==> INIZIO Prodcedura_2");
                    //AZIONE 2 (Riepiloghi NOTE SPESE)
                    procedura_2(_dt_CSV, _dt_XSLX, _oNespressoProject);
                    log.Info(" ==> FINE Prodcedura_2");

                    log.Info(" ==> INIZIO CREAZIONE XLS Conservazione");
                    //CONSERVAZIONE A NORMA
                    Creazione_CSV_Conservazione(_oNespressoProject);
                    log.Info(" ==> FINE CREAZIONE XLS Conservazione");
                    //CONSERVAZIONE FILE ERRORI
                    log.Info(" ==> INIZIO CREAZIONE XLS ERRORI");
                    Creazione_CSV_ConservazioneErrori(_oNespressoProject);
                    log.Info(" ==> FINE CREAZIONE XLS ERRORI");
                }
            }
            catch (Exception ex)
            {
                Errore_List.Add(ex.Message);
                log.Error(ex.Message);
                log.Error(ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        ///AZIONE 1 (Giustificativi): Partendo dal file CSV guida, e per ogni record in esso contenuto, 
        ///utilizzare il campo n. 27 (colonna AA se si apre con Excel) per cercare il corrispondente record del 
        ///movimento nel file Excel dati, in colonna 31 o AJ.
        ///-Se trovo il record genero una riga in un nuovo file CSV, dopo aver verificato che sia presente nella 
        ///directory immagini giustificativi il file indicato nel file guida, così composto:
        ///1)	Nome file. Rilevato dal CSV originale nel campo 4 (colonna D se si apre con Excel)
        ///2)	ID Nota Spese. Colonna D Excel
        ///3)	ID Dipendente.  Colonna I Excel
        ///4)	Nome Dipendente. Colonna J
        ///5)	Cognome Dipendente. Colonna K
        ///6)	Descrizione transazione. Colonna AI
        ///7)	ID transazione. Colonna AJ
        ///8)	Data transazione. Colonna AK
        ///9)	Importo in EUR. Colonna EP
        ///10)	Tipo Documento ( fisso=Giustificativo)
        ///11)	Anno riferimento
        ///12)	Mese riferimento
        ///Es: 
        ///1)	21590_PNG_20190109_161704_1706962409028379796.png
        ///2)	5084/009330
        ///3)	1199
        ///4)	VIVIANA
        ///5)	ROSSI
        ///6)	blocchetto pony express (campo valido solo per giustificativo)
        ///7)	5007624342 (campo valido solo per giustificativo)
        ///8)	09/01/2019 (campo valido solo per giustificativo)
        ///9)	-121,18
        ///10)	Giustificativo
        ///11)	2019
        ///12)	01
        ///-Se non trovo nessun record genero un record in un file di errore dove segnalo il numero 
        ///del record non trovato riportando tutto il record del file guida, e la stringa “Record non trovato”
        ///-Se trovo il record ma non il file immagine genero un record in un file di errore dove segnalo
        ///il numero del record di cui non ho trovato l’immagine, e la stringa “Immagine non trovata”.
        /// </summary>
        /// <param name="_dt_CSV">Source contenente i dati del CSV</param>
        /// <param name="_dt_XSLX">Source contenente i dati dell'excel</param>
        /// <param name="_oNespressoProject">Oggetto di configurazione</param>
        private void procedura_1(DataTable _dt_CSV, DataTable _dt_XSLX, NESPRESSO_Project _oNespressoProject)
        {
            MD_NOTA_SPESE oMD_NOTA_SPESE = new MD_NOTA_SPESE();
            System.String ExpenseID = string.Empty;

            try
            {
                foreach (DataRow _dr_CSV in _dt_CSV.Rows)
                {
                    log.Info($"==============================");
                    oMD_NOTA_SPESE = new MD_NOTA_SPESE();
                    try
                    {

                        ExpenseID = _dr_CSV["ExpenseID"] == DBNull.Value ? String.Empty : _dr_CSV["ExpenseID"].ToString();

                        if (string.IsNullOrEmpty(ExpenseID))
                        {
                            throw new Exception($"Errore CSV - ExpenseID Vuoto per il file {_dr_CSV["fileName"].ToString()}");
                        }
                        else
                        {
                            log.Info($"Expense ID analizzazto {ExpenseID}");
                            System.String _pathFile = string.Empty;
                            //Verifico la presenza del file
                            _pathFile = Path.Combine(_oNespressoProject.NESPRESSO_Input_WORK, "IMMAGINI", _dr_CSV["fileName"].ToString());
                            if (File.Exists(_pathFile))
                            {
                                log.Info($"File verificato con parh : {_pathFile}");
                                DataRow _rowxls = _dt_XSLX.Select($"[ID Transazione] ='{ExpenseID}'").FirstOrDefault();

                                if (_rowxls != null)
                                {
                                    log.Info($"ID Transazione {ExpenseID} trovati record numero : 1");
                                        
                                        if (File.Exists(_pathFile))
                                        {
                                            FileInfo _fileToSave = new FileInfo(Path.Combine(_oNespressoProject.NESPRESSO_Input_WORK, "IMMAGINI", _dr_CSV["fileName"].ToString()));

                                            oMD_NOTA_SPESE.NOME_FILE = _dr_CSV["fileName"] == DBNull.Value ? String.Empty : _dr_CSV["fileName"].ToString();

                                            oMD_NOTA_SPESE.ID_NOTA_SPESE = _rowxls["ID Nota Spese"] == DBNull.Value ? String.Empty : _rowxls["ID Nota Spese"].ToString();
                                            oMD_NOTA_SPESE.ID_DIPENDENTE = _rowxls["ID Dipendente"] == DBNull.Value ? String.Empty : _rowxls["ID Dipendente"].ToString();
                                            oMD_NOTA_SPESE.NOME_DIPENDENTE = _rowxls["Nome del Dipendente"] == DBNull.Value ? String.Empty : _rowxls["Nome del Dipendente"].ToString();
                                            oMD_NOTA_SPESE.COGNOME_DIPENDENTE = _rowxls["Cognome del Dipendente"] == DBNull.Value ? String.Empty : _rowxls["Cognome del Dipendente"].ToString();
                                            oMD_NOTA_SPESE.DESCRIZIONE_TRANSAZIONE = _rowxls["Descrizione Transazione"] == DBNull.Value ? String.Empty : _rowxls["Descrizione Transazione"].ToString();
                                            oMD_NOTA_SPESE.ID_TRANSAZIONE = _rowxls["ID Transazione"] == DBNull.Value ? String.Empty : _rowxls["ID Transazione"].ToString();
                                            oMD_NOTA_SPESE.DATA_TRANSAZIONE = _rowxls["Data della transazione"] == DBNull.Value ? String.Empty : _rowxls["Data della transazione"].ToString();
                                            oMD_NOTA_SPESE.IMPORTO = _rowxls["Importo in EUR"] == DBNull.Value ? String.Empty : Convert.ToDecimal(_rowxls["Importo in EUR"].ToString()).ToString();
                                            oMD_NOTA_SPESE.TIPO_DOCUMENTO = "Giustificativo";
                                            System.DateTime _data = Convert.ToDateTime(_rowxls["Periodo"].ToString());
                                            oMD_NOTA_SPESE.ANNO_RIFERIMENTO = _data.Year.ToString();
                                            oMD_NOTA_SPESE.MESE_RIFERIMENTO = _data.Month.ToString().PadLeft(2,'0');
                                            oMD_NOTA_SPESE.PATH_FULL_FILE = _fileToSave.FullName;
                                            _List_MD_NOTA_SPESE.Add(oMD_NOTA_SPESE);

                                            log.Info($" ID_NOTA_SPESE : { oMD_NOTA_SPESE.ID_NOTA_SPESE}");
                                            log.Info($" ID_DIPENDENTE : { oMD_NOTA_SPESE.ID_DIPENDENTE}");
                                            log.Info($" NOME_DIPENDENTE : { oMD_NOTA_SPESE.NOME_DIPENDENTE}");
                                            log.Info($" COGNOME_DIPENDENTE : { oMD_NOTA_SPESE.COGNOME_DIPENDENTE}");
                                        }
                                }
                                else
                                {
                                    log.Info($"ID Transazione {ExpenseID} NON TROVATO NESSUN RECORD");
                                    String _FileGuida = string.Join(",", _dr_CSV.ItemArray);
                                    _List_MD_NOTA_SPESE_ERRORI.Add(new MD_NOTA_SPESE_ERRORI
                                    {
                                        NUMERO_RECORD = ExpenseID,
                                        NOME_FILE = _dr_CSV["fileName"].ToString(),
                                        RECORD_FILE_GUIDA = _FileGuida,
                                        DESCRIZIONE_ERRORI = "Record Non Trovato"
                                    });
                                }
                            }
                            else
                            {
                                log.Info($"IMMAGINE NON TROVATA : {_pathFile}");
                                String _FileGuida = string.Join(",", _dr_CSV.ItemArray);
                                _List_MD_NOTA_SPESE_ERRORI.Add(new MD_NOTA_SPESE_ERRORI
                                {
                                    NUMERO_RECORD = ExpenseID,
                                    NOME_FILE = _dr_CSV["fileName"].ToString(),
                                    RECORD_FILE_GUIDA = _FileGuida,
                                    DESCRIZIONE_ERRORI = "Immagine Non Trovata"
                                });
                            }
                        }
                    }
                    catch (Exception)
                    {

                        log.Error($"Procedura 1 :Errore sul nome file: {oMD_NOTA_SPESE.NOME_FILE} valore di Ricerca ExpenseID {ExpenseID}");
                        Errore_List.Add($"Procedura 1 :Errore sul file: {oMD_NOTA_SPESE.NOME_FILE} valore di Ricerca ExpenseID {ExpenseID}");
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void procedura_2(DataTable _dt_CSV, DataTable _dt_XSLX, NESPRESSO_Project _oNespressoProject)
        {
            System.String _pathPDF = Path.Combine(_oNespressoProject.NESPRESSO_Input_WORK, "PDF");
            try
            {
                if (!Directory.Exists(_pathPDF))
                {
                    log.Error("Procedura 2 :Cartella PDF non esistente");
                    throw new Exception("Procedura 2 :Cartella PDF non esistente");
                }

                DirectoryInfo _dirPDF = new DirectoryInfo(_pathPDF);
                //estraggo tutti i file contenuti nella cartella
                List<FileInfo> _ListFileInfo = _dirPDF.GetFiles().ToList();

                if (_ListFileInfo != null && _ListFileInfo.Count > 0)
                {
                    log.Info($"Numero di files da lavorare : {_ListFileInfo.Count}");
                    System.String _nameFile = String.Empty;
                    System.String _searchCode = string.Empty;
                    foreach (FileInfo _filePDF in _ListFileInfo)
                    {
                        log.Info($"==============================");
                        MD_NOTA_SPESE oMD_NOTA_SPESE = null;
                        try
                        {
                            _nameFile = Path.GetFileNameWithoutExtension(_filePDF.Name);
                            log.Info($"Nome File da Lavorare {_filePDF.Name}");

                            _searchCode = String.Concat(_nameFile.Split('_')[6].ToString(), "/", _nameFile.Split('_')[7].ToString());
                            log.Info($"Codice di ricerca {_searchCode}");

                            DataRow _rowxls = _dt_XSLX.Select($"[ID Nota Spese] ='{_searchCode}'").FirstOrDefault();

                            if (_rowxls != null)
                            {
                                oMD_NOTA_SPESE = new MD_NOTA_SPESE();

                                oMD_NOTA_SPESE.NOME_FILE = _filePDF.Name;
                                oMD_NOTA_SPESE.ID_NOTA_SPESE = _rowxls["ID Nota Spese"] == DBNull.Value ? String.Empty : _rowxls["ID Nota Spese"].ToString();
                                oMD_NOTA_SPESE.ID_DIPENDENTE = _rowxls["ID Dipendente"] == DBNull.Value ? String.Empty : _rowxls["ID Dipendente"].ToString();
                                oMD_NOTA_SPESE.NOME_DIPENDENTE = _rowxls["Nome del Dipendente"] == DBNull.Value ? String.Empty : _rowxls["Nome del Dipendente"].ToString();
                                oMD_NOTA_SPESE.COGNOME_DIPENDENTE = _rowxls["Cognome del Dipendente"] == DBNull.Value ? String.Empty : _rowxls["Cognome del Dipendente"].ToString();
                                oMD_NOTA_SPESE.DESCRIZIONE_TRANSAZIONE = String.Empty;
                                oMD_NOTA_SPESE.ID_TRANSAZIONE = String.Empty;
                                oMD_NOTA_SPESE.DATA_TRANSAZIONE = String.Empty;
                                oMD_NOTA_SPESE.IMPORTO = String.Empty;
                                oMD_NOTA_SPESE.TIPO_DOCUMENTO = "Nota Spese";
                                System.DateTime _data = Convert.ToDateTime(_rowxls["Periodo"].ToString());
                                oMD_NOTA_SPESE.ANNO_RIFERIMENTO = _data.Year.ToString();
                                oMD_NOTA_SPESE.MESE_RIFERIMENTO = _data.Month.ToString().PadLeft(2, '0');
                                oMD_NOTA_SPESE.PATH_FULL_FILE = _filePDF.FullName;

                                log.Info($" ID_NOTA_SPESE : { oMD_NOTA_SPESE.ID_NOTA_SPESE}");
                                log.Info($" ID_DIPENDENTE : { oMD_NOTA_SPESE.ID_DIPENDENTE}");
                                log.Info($" NOME_DIPENDENTE : { oMD_NOTA_SPESE.NOME_DIPENDENTE}");
                                log.Info($" COGNOME_DIPENDENTE : { oMD_NOTA_SPESE.COGNOME_DIPENDENTE}");
                                
                                _List_MD_NOTA_SPESE.Add(oMD_NOTA_SPESE);
                            }
                            else
                            {
                                log.Info($" _searchCode Non Trovato : { _searchCode}");
                                _List_MD_NOTA_SPESE_ERRORI.Add(new MD_NOTA_SPESE_ERRORI
                                {
                                    NUMERO_RECORD = _searchCode,
                                    NOME_FILE = _filePDF.Name,
                                    RECORD_FILE_GUIDA = "",
                                    DESCRIZIONE_ERRORI = "Record Non Trovato"
                                });
                            }
                        }
                        catch (Exception)
                        {

                            log.Error($"Procedura 2 :Errore sul nome file: {oMD_NOTA_SPESE.NOME_FILE}");
                            Errore_List.Add($"Procedura 2 :Errore sul file: {oMD_NOTA_SPESE.NOME_FILE}");
                        }

                    }
                }
                else
                {
                    log.Info("Nessun File Presente nella Cartella PDF");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        ///Punto 3: CONSERVAZIONE A NORMA
        ///Il Soggetto Produttore è “Nespresso”.
        ///Si dovrà creare un nuovo profilo di archiviazione denominato “Note Spese”.
        ///La conservazione a norma sarà guidata da un file indice CSV così composto:
        ///1)	Nome file
        ///2)	ID Nota Spese
        ///3)	ID Dipendente
        ///4)	Nome Dipendente
        ///5)	Cognome Dipendente
        ///6)	Descrizione transazione
        ///7)	ID transazione
        ///8)	Data transazione
        ///9)	Importo in EUR
        ///10)	Tipo Documento
        ///11)	Anno riferimento
        ///12)	Mese riferimento
        /// </summary>
        /// <param name="_oNespressoProject">Oggetto configurazione</param>
        private void Creazione_CSV_Conservazione(NESPRESSO_Project _oNespressoProject)
        {
            try
            {
                //Verifico se vi sono dei valori presenti all'interno dell'elenco
                if (_List_MD_NOTA_SPESE.Count > 0)
                {
                    MD_Excel _MD_Excel = new MD_Excel();
                    _MD_Excel.GenerateExcelConservazione(_List_MD_NOTA_SPESE, _oNespressoProject);

                }
                else
                {
                    log.Info("Generazione del file per la conservatoria non effettuato- Assenza dati");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Creazione_CSV_ConservazioneErrori(NESPRESSO_Project _oNespressoProject)
        {
            try
            {
                //Verifico se vi sono dei valori presenti all'interno dell'elenco
                if (_List_MD_NOTA_SPESE_ERRORI.Count > 0)
                {
                    MD_Excel _MD_Excel = new MD_Excel();
                    _MD_Excel.GenerateExcelErrori(_List_MD_NOTA_SPESE_ERRORI, _oNespressoProject);

                }
                else
                {
                    log.Info("Generazione del file ERRORI per la conservatoria non effettuato- Assenza dati");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
    class MD_NOTA_SPESE
    {
        //Colonna 4 foglio CSV
        public System.String NOME_FILE { get; set; }
        //Colonna D del foglio Excel
        public System.String ID_NOTA_SPESE { get; set; }
        //Colonna I del foglio Excel
        public System.String ID_DIPENDENTE { get; set; }
        //Colonna J del foglio Excel
        public System.String NOME_DIPENDENTE { get; set; }
        //Colonna K del foglio Excel
        public System.String COGNOME_DIPENDENTE { get; set; }
        //Colonna AI del foglio Excel
        public System.String DESCRIZIONE_TRANSAZIONE { get; set; }
        //Colonna AJ del foglio Excel
        public System.String ID_TRANSAZIONE { get; set; }
        //Colonna AK del foglio Excel
        public System.String DATA_TRANSAZIONE { get; set; }
        //Colonna EP del foglio Excel
        public System.String IMPORTO { get; set; }
        //{Flusso = Giustificativo/Tipo Documento}
        public System.String TIPO_DOCUMENTO { get; set; }

        public System.String ANNO_RIFERIMENTO { get; set; }

        public System.String MESE_RIFERIMENTO { get; set; }
        //Uso questo campo per avere a disposizione tutto il path del file da ricercare
        public System.String PATH_FULL_FILE { get; set; }
    }

    class MD_NOTA_SPESE_ERRORI
    {

        public System.String NUMERO_RECORD { get; set; }

        public System.String NOME_FILE { get; set; }

        public System.String RECORD_FILE_GUIDA { get; set; }

        public System.String DESCRIZIONE_ERRORI { get; set; }

    }
}
