﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using MD.Configuration;

namespace MD.Zip
{
    public class MD_Zip
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public string extract_Zip(String Crif_inputZip, System.String CRIF_Input_WORK)
        {
            String _pathextract = String.Empty;
            try
            {
                using (ZipFile zip = ZipFile.Read(Crif_inputZip))
                {
                    
                    if (zip.Entries.Count != 0)
                    {
                        log.Info($"Estrazione File Zip : {zip.Name}");
                        foreach (ZipEntry _zipentry in zip)
                        {
                            log.Info($"File contenuti nello Zip : {_zipentry.FileName}");

                        }
                        _pathextract = Path.Combine(CRIF_Input_WORK, Path.GetFileNameWithoutExtension(zip.Name));
                        Directory.CreateDirectory(_pathextract);
                        zip.ExtractAll(_pathextract, ExtractExistingFileAction.OverwriteSilently);
                    }
                    else
                    {
                        log.Error($"Zip : {zip.Name} senza nessun file al suo interno.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _pathextract;
        }


        //#region "ExtractList_Zip"

        ///// <summary>
        ///// Estraggo l'elenco dei file presenti all'interno della cartella di SFPT
        ///// </summary>
        ///// <returns></returns>
        //public List<FileInfo> ExtractList_Zip()
        //{
        //    List<FileInfo> _List_Zip = new List<FileInfo> { };
        //    try
        //    {
        //        if (!Directory.Exists(MD_Configuration.CRIF_Input_SFTP)) { log.Error($"Path SFTP non presente : {Crif_Configuration.CRIF_Input_SFTP}"); }

        //        DirectoryInfo _DIR_CRIF_Input_SFTP = new DirectoryInfo(Crif_Configuration.CRIF_Input_SFTP);
        //        //_List_Zip = _DIR_CRIF_Input_SFTP.GetFiles("*.*").Where(file => string.Equals(file.Extension, ".zip", StringComparison.InvariantCultureIgnoreCase) 
        //        //                                                        || string.Equals(file.Extension, ".rar", StringComparison.InvariantCultureIgnoreCase)
        //        //                                                        || string.Equals(file.Extension, ".csv", StringComparison.InvariantCultureIgnoreCase)).ToList();
        //        if (Crif_Configuration.CRIF_CERTIFICATO_PGP_Enable)
        //        {
        //            _List_Zip = _DIR_CRIF_Input_SFTP.GetFiles("*.*").Where(file => string.Equals(file.Extension.ToUpper(), ".PGP", StringComparison.InvariantCultureIgnoreCase)).ToList();
        //        }
        //        else
        //        {
        //            _List_Zip = _DIR_CRIF_Input_SFTP.GetFiles("*.*").Where(file => string.Equals(file.Extension.ToUpper(), ".ZIP", StringComparison.InvariantCultureIgnoreCase) ||
        //               string.Equals(file.Extension.ToUpper(), ".CSV", StringComparison.InvariantCultureIgnoreCase)).ToList();
        //        }
        //        if (_List_Zip == null || _List_Zip.Count == 0) { log.Info($"Nessun File presente nella cartella {Crif_Configuration.CRIF_Input_SFTP}"); }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return _List_Zip;
        //}
        //#endregion
        
        /// <summary>
        /// Copia del file arrivato criptato
        /// </summary>
        /// <param name="_fileZip">file info</param>
        //public void Copy_FileEnCrypted(FileInfo _fileZip)
        //{
        //    try
        //    {
        //        if (!Directory.Exists(Crif_Configuration.CRIF_Input_SFTP_BACKUP)) { Directory.CreateDirectory(Crif_Configuration.CRIF_Input_SFTP_BACKUP); }
        //        if (File.Exists(_fileZip.FullName))
        //        {
        //            File.Copy(_fileZip.FullName,Path.Combine(Crif_Configuration.CRIF_Input_SFTP_BACKUP, _fileZip.Name), true);
        //            log.Info($"File Encrypted Copiato In backUp correttamente {Crif_Configuration.CRIF_Input_SFTP_BACKUP} File {_fileZip.Name}");
        //        }
        //        else
        //        {
        //            log.Error($" File Encrypted non copiato correttamente {Crif_Configuration.CRIF_Input_SFTP_BACKUP} File {_fileZip.Name}");
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public void Zip_Directory(System.String _DirectoryName, System.String _PathOutput ) {
            try
            {
                ZipFile _zipFile = new ZipFile();
                _zipFile.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                //_zipFile.CompressionMethod = CompressionMethod.BZip2;
                _zipFile.AddDirectory(_DirectoryName);
                
                _zipFile.Save(Path.Combine(_PathOutput,"Archive.zip"));
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Zip_File(List<FileInfo> _listFile, System.String _PathOutput)
        {
            try
            {
                ZipFile _zipFile = new ZipFile();
                foreach (var _fileinfo in _listFile) {
                    _zipFile.AddFile(_fileinfo.FullName);
                }                                
                _zipFile.Save(Path.Combine(_PathOutput, "Archive.zip"));
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
