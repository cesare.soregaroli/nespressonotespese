﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MD.DevExpressMdg;
using System.IO;
using MD.Configuration;
using DevExpress.Spreadsheet;
using NESPRESSO_NOTESPESE.XMLConfigurazione;
using MD.Elaborazione;
using NLog.Fluent;

namespace MD.ExcelMDG
{
    class MD_Excel
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public System.Data.DataTable load_CSV(System.String _pathExcel, System.Int16 _idSheet, System.Char _separatore)
        {

            System.Data.DataTable _dtKYC_Scadenziario = null;
            try
            {
                ExpressExcelMdg oDevExpressExcel = new ExpressExcelMdg();
                oDevExpressExcel.loadDocument(_pathExcel, DevExpress.Spreadsheet.DocumentFormat.Csv, _separatore);
                _dtKYC_Scadenziario = oDevExpressExcel.ExportDataTable(_idSheet);

            }
            catch (Exception)
            {
                throw;
            }
            return _dtKYC_Scadenziario;
        }

        public System.Data.DataTable load_XSLS(System.String _pathExcel, System.Int16 _idSheet, System.Int16 _rowDeleted)
        {

            System.Data.DataTable _dtKYC_Scadenziario = null;
            try
            {
                ExpressExcelMdg oDevExpressExcel = new ExpressExcelMdg();
                oDevExpressExcel.loadDocument(_pathExcel, DocumentFormat.Xls);
                _dtKYC_Scadenziario = oDevExpressExcel.ExportDataTableRemoveRow(_idSheet, _rowDeleted);
            }
            catch (Exception)
            {
                throw;
            }
            return _dtKYC_Scadenziario;
        }

        

        public void GenerateExcelConservazione(List<MD_NOTA_SPESE> _List_MD_NOTA_SPESE, NESPRESSO_Project _oNespressoProject)
        {
            String _nameFilexlsx = $"{System.DateTime.Now.ToString("yyyyMMdd")}_Nespresso_Note_Spese.xlsx";
            String _nameFileCSV = $"{System.DateTime.Now.ToString("yyyyMMdd")}_Nespresso_Note_Spese.csv";
            System.String _PathExcel = Path.Combine(_oNespressoProject.NESPRESSO_OUTPUT_CONSERVAZIONE, _nameFilexlsx);
            System.String _PathCSV = Path.Combine(_oNespressoProject.NESPRESSO_OUTPUT_CONSERVAZIONE, _nameFileCSV);
            System.String _DataReview = System.DateTime.Now.ToString("yyyyMMdd");
            System.String _PathDepositoFiles = Path.Combine(_oNespressoProject.NESPRESSO_OUTPUT_CONSERVAZIONE, "Files");
            try
            {
                if (!Directory.Exists(_PathDepositoFiles)) { Directory.CreateDirectory(_PathDepositoFiles); }
                if (_List_MD_NOTA_SPESE != null && _List_MD_NOTA_SPESE.Count > 0)
                {
                    System.Data.DataTable _dtProduzione = new System.Data.DataTable();
                    _dtProduzione.Columns.Add("NOME_FILE", typeof(String));
                    _dtProduzione.Columns.Add("ID_NOTA_SPESE", typeof(String));
                    _dtProduzione.Columns.Add("ID_DIPENDENTE", typeof(String));
                    _dtProduzione.Columns.Add("NOME_DIPENDENTE", typeof(String));
                    _dtProduzione.Columns.Add("COGNOME_DIPENDENTE", typeof(String));
                    _dtProduzione.Columns.Add("DESCRIZIONE_TRANSAZIONE", typeof(String));
                    _dtProduzione.Columns.Add("ID_TRANSAZIONE", typeof(String));
                    _dtProduzione.Columns.Add("DATA_TRANSAZIONE", typeof(String));
                    _dtProduzione.Columns.Add("IMPORTO", typeof(String));
                    _dtProduzione.Columns.Add("TIPO_DOCUMENTO", typeof(String));
                    _dtProduzione.Columns.Add("ANNO_RIFERIMENTO", typeof(String));
                    _dtProduzione.Columns.Add("MESE_RIFERIMENTO", typeof(String));
                   //_dtProduzione.Columns.Add("PATH_FULL_FILE", typeof(String));

                    foreach (MD_NOTA_SPESE _MD_NOTA_SPESE in _List_MD_NOTA_SPESE)
                    {
                        var _rowProduzione = _dtProduzione.NewRow();

                        _rowProduzione["NOME_FILE"] = _MD_NOTA_SPESE.NOME_FILE;
                        _rowProduzione["ID_NOTA_SPESE"] = _MD_NOTA_SPESE.ID_NOTA_SPESE;
                        _rowProduzione["ID_DIPENDENTE"] = _MD_NOTA_SPESE.ID_DIPENDENTE;
                        _rowProduzione["NOME_DIPENDENTE"] = _MD_NOTA_SPESE.NOME_DIPENDENTE;
                        _rowProduzione["COGNOME_DIPENDENTE"] = _MD_NOTA_SPESE.COGNOME_DIPENDENTE;
                        _rowProduzione["DESCRIZIONE_TRANSAZIONE"] = _MD_NOTA_SPESE.DESCRIZIONE_TRANSAZIONE;
                        _rowProduzione["ID_TRANSAZIONE"] = _MD_NOTA_SPESE.ID_TRANSAZIONE;
                        _rowProduzione["DATA_TRANSAZIONE"] = _MD_NOTA_SPESE.DATA_TRANSAZIONE;
                        _rowProduzione["IMPORTO"] = _MD_NOTA_SPESE.IMPORTO;
                        _rowProduzione["TIPO_DOCUMENTO"] = _MD_NOTA_SPESE.TIPO_DOCUMENTO;
                        _rowProduzione["ANNO_RIFERIMENTO"] = _MD_NOTA_SPESE.ANNO_RIFERIMENTO;
                        _rowProduzione["MESE_RIFERIMENTO"] = _MD_NOTA_SPESE.MESE_RIFERIMENTO;
                        //_rowProduzione["PATH_FULL_FILE"] = _MD_NOTA_SPESE.PATH_FULL_FILE;
                        FileInfo _fileinfo = new FileInfo(_MD_NOTA_SPESE.PATH_FULL_FILE);

                        log.Info($"Nome Files : {_MD_NOTA_SPESE.NOME_FILE}");
                        log.Info($"Path File : {_MD_NOTA_SPESE.PATH_FULL_FILE}");

                        if (!File.Exists(Path.Combine(_PathDepositoFiles, _fileinfo.Name))) { 
                        File.Copy(_MD_NOTA_SPESE.PATH_FULL_FILE, Path.Combine(_PathDepositoFiles, _fileinfo.Name));
                        }

                        _dtProduzione.Rows.Add(_rowProduzione);
                    }

                    if (_dtProduzione != null && _dtProduzione.Rows.Count > 0)
                    {
                        ExpressExcelMdg oDevExpressExcel = new ExpressExcelMdg();
                        oDevExpressExcel.RenameSheet(0, _DataReview);
                        oDevExpressExcel.AddTextInCell(_DataReview, _dtProduzione,true);
                        oDevExpressExcel.SaveDocument(_PathExcel);                        

                        ExpressExcelMdg oDevExpressCSV = new ExpressExcelMdg();
                        oDevExpressCSV.RenameSheet(0, _DataReview);
                        oDevExpressCSV.AddTextInCell(_DataReview, _dtProduzione,false);
                        oDevExpressCSV.SaveDocumentCSV(_PathCSV, '|');

                        //Se il file è creato correttamente effettuo il backup nella cartella preposta
                        if (new FileInfo(_PathExcel).Exists)
                        {
                             if (!Directory.Exists(_oNespressoProject.NESPRESSO_BACKUP_CONSERVAZIONE)) { Directory.CreateDirectory(_oNespressoProject.NESPRESSO_BACKUP_CONSERVAZIONE); }
                            File.Copy(_PathExcel, Path.Combine(_oNespressoProject.NESPRESSO_BACKUP_CONSERVAZIONE, new FileInfo(_PathExcel).Name));
                            File.Copy(_PathCSV, Path.Combine(_oNespressoProject.NESPRESSO_BACKUP_CONSERVAZIONE, new FileInfo(_PathCSV).Name));
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void GenerateExcelErrori(List<MD_NOTA_SPESE_ERRORI> _List_MD_NOTA_SPESE_ERRORI, NESPRESSO_Project _oNespressoProject)
        {
            String _nameFile = $"{System.DateTime.Now.ToString("yyyyMMdd")}_Nespresso_Note_Spese_ERRORI.xlsx";
            System.String _PathExcel = Path.Combine(_oNespressoProject.NESPRESSO_OUTPUT_CONSERVAZIONE, _nameFile);
            System.String _DataReview = System.DateTime.Now.ToString("yyyyMMdd");            
            try
            {
                
                if (_List_MD_NOTA_SPESE_ERRORI != null && _List_MD_NOTA_SPESE_ERRORI.Count > 0)
                {

                    System.Data.DataTable _dtProduzione = new System.Data.DataTable();
                    _dtProduzione.Columns.Add("NUMERO_RECORD", typeof(String));
                    _dtProduzione.Columns.Add("NOME_FILE", typeof(String));                    
                    _dtProduzione.Columns.Add("RECORD_FILE_GUIDA", typeof(String));
                    _dtProduzione.Columns.Add("DESCRIZIONE_ERRORI", typeof(String));
                    

                    foreach (MD_NOTA_SPESE_ERRORI _MD_NOTA_SPESE in _List_MD_NOTA_SPESE_ERRORI)
                    {
                        var _rowProduzione = _dtProduzione.NewRow();

                        _rowProduzione["NUMERO_RECORD"] = _MD_NOTA_SPESE.NUMERO_RECORD;
                        _rowProduzione["NOME_FILE"] = _MD_NOTA_SPESE.NOME_FILE;
                        _rowProduzione["RECORD_FILE_GUIDA"] = _MD_NOTA_SPESE.RECORD_FILE_GUIDA;
                        _rowProduzione["DESCRIZIONE_ERRORI"] = _MD_NOTA_SPESE.DESCRIZIONE_ERRORI;
                                                
                        _dtProduzione.Rows.Add(_rowProduzione);
                    }

                    if (_dtProduzione != null && _dtProduzione.Rows.Count > 0)
                    {
                        ExpressExcelMdg oDevExpressExcel = new ExpressExcelMdg();
                        oDevExpressExcel.RenameSheet(0, _DataReview);
                        oDevExpressExcel.AddTextInCell(_DataReview, _dtProduzione,true);
                        oDevExpressExcel.SaveDocument(_PathExcel);

                        //Se il file è creato correttamente effettuo il backup nella cartella preposta
                        if (new FileInfo(_PathExcel).Exists)
                        {
                            if (!Directory.Exists(_oNespressoProject.NESPRESSO_BACKUP_CONSERVAZIONE)) { Directory.CreateDirectory(_oNespressoProject.NESPRESSO_BACKUP_CONSERVAZIONE); }
                            File.Copy(_PathExcel, Path.Combine(_oNespressoProject.NESPRESSO_BACKUP_CONSERVAZIONE, new FileInfo(_PathExcel).Name));
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
