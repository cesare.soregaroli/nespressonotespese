﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using MD.Errore;
using MD.Configuration;

namespace MD.Mail
{
    public class MD_Mail
    {
        public System.String email_smtpserver { get; set; }
        public System.Boolean email_sendemail { get; set; }
        public System.String email_from { get; set; }
        public System.String email_to { get; set; }
        public System.String email_cc { get; set; }
        public System.String email_eccezione_to { get; set; }
        public System.String email_path_folder { get; set; }
        public System.String email_name_file { get; set; }
        public List<System.String> email_Attachement { get; set; }

        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();


        public void SendMailLog()
        {
            MailMessage message = null;
            SmtpClient client = null;
            try
            {
                message = new MailMessage(this.email_from, this.email_to);
                message.Subject = $" DEUT - Nespresso_Note_Spese - Execution Log ";
                message.Body = preparataTestoMailLog().ToString();

                client = new SmtpClient(this.email_smtpserver);
                client.UseDefaultCredentials = true;

                if (email_Attachement.Count > 0)
                {
                    foreach (var _path in email_Attachement)
                    {
                        message.Attachments.Add(new Attachment(_path));
                    }
                }
                client.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }

        /// <summary>
        /// Invio della mail contente errori
        /// </summary>
        /// <param name="ex">Eccezione</param>
        /// <param name="_microframe">Abilito l'invio a Microframe dell'eccezione</param>
        public void SendMailError(Boolean _microframe)
        {
            MailMessage message = null;
            SmtpClient client = null;
            try
            {
                if (email_sendemail)
                {
                    if (_microframe)
                    {
                        message = new MailMessage(this.email_from, this.email_to);
                        message.CC.Add(email_cc);
                    }
                    else
                    {
                        message = new MailMessage(this.email_from, this.email_eccezione_to);
                    }
                    message.Subject = $" Deutsche Bank - NESPRESSO NOTE SPESE";
                    if (_microframe)
                    {
                        message.Body = preparataTestoMail().ToString();
                    }
                    else
                    {
                        message.Body = preparataTestoMailStackTrace().ToString();
                    }
                    client = new SmtpClient(this.email_smtpserver);
                    client.UseDefaultCredentials = true;
                    if (_microframe)
                    {
                        if (email_Attachement.Count > 0)
                        {
                            foreach (var _path in email_Attachement)
                            {
                                message.Attachments.Add(new Attachment(_path));
                            }
                        }
                    }

                    client.Send(message);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }


        private System.Text.StringBuilder preparataTestoMail()
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset di Riferimento: NESPRESSO_NOTE_SPESE{System.Environment.NewLine} Referente : Soregaroli C.");
                strMail.Append($"{System.Environment.NewLine}Elaborazione terminata con Errori.");
                strMail.Append($"{System.Environment.NewLine}Verificare il Log presente sull'Host :{Environment.MachineName}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }

        private System.Text.StringBuilder preparataTestoMailLog()
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset di Riferimento: NESPRESSO_NOTE_SPESE{System.Environment.NewLine} Referente : Soregaroli C.");
                strMail.Append($"{System.Environment.NewLine}Verificare il Log presente sull'Host :{Environment.MachineName}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }

        private System.Text.StringBuilder preparataTestoMail(System.String _Messaggio)
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset Di Riferimento: NESPRESSO_NOTE_SPESE{System.Environment.NewLine} Referente : Soregaroli C.");
                strMail.Append($"Alert Generated : {_Messaggio}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }

        private System.Text.StringBuilder preparataTestoMailStackTrace()
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset di Riferimento: NESPRESSO_NOTE_SPESE{System.Environment.NewLine} Referente : Soregaroli C.");
                strMail.Append($"{System.Environment.NewLine}Elaborazione terminata con Errori.");
                strMail.Append($"{System.Environment.NewLine}Verificare il Log presente sull'Host : {Environment.MachineName}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }
        /// <summary>
        /// Metodo per salvare in un file l'elenco degli errori
        /// </summary>
        /// <param name="_crif_Errori"></param>
        /// <returns></returns>
        public System.String CreateLogfile(List<System.String> Errore_List)
        {

            System.String _logfile = Path.Combine(MD_Configuration.TOOL_LOG_ERROR, $"Log_File_Error_{System.DateTime.Now.Millisecond}.txt");
            try
            {
                if (!Directory.Exists(MD_Configuration.TOOL_LOG_ERROR)) { Directory.CreateDirectory(MD_Configuration.TOOL_LOG_ERROR); }
                else
                {
                    var _dirInfo = new DirectoryInfo(MD_Configuration.TOOL_LOG_ERROR);
                    foreach (System.IO.FileInfo file in _dirInfo.GetFiles()) file.Delete();
                }

                if (!File.Exists(_logfile))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(_logfile))
                    {
                        sw.WriteLine(string.Join(Environment.NewLine, Errore_List.ToArray()));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _logfile;
        }


    }
}
