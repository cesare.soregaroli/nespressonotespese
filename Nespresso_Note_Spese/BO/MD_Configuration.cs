﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NESPRESSO_NOTESPESE.XMLConfigurazione;

namespace MD.Configuration
{
    static class MD_Configuration
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string _data = DateTime.Now.ToString("yyyyMMdd_HHmmss");

        /// <summary>
        /// Path XML di configurazione
        /// </summary>
        static public string TOOL_PATH_XML
        {
            get => System.Configuration.ConfigurationManager.AppSettings["TOOL_PATH_XML"].ToString();
        }

        /// <summary>
        /// Path LOG di errore
        /// </summary>        
        static public string TOOL_LOG_ERROR
        {
            get => System.Configuration.ConfigurationManager.AppSettings["TOOL_LOG_ERROR"].ToString();
        }

        /// <summary>
        /// Numero di righe da eliminare dall'excel
        /// </summary>
        static public System.Int16 TOOL_ROW_XSL_DELETE
        { 
            get => Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TOOL_ROW_XSL_DELETE"].ToString());
        }


        /// <summary>
        /// Stampo Il contenuto dei parametri settati
        /// </summary>
        static public void LogParameterAttribute()
        {
            try
            {
                log.Info($"Path EXPORT_TOOL_PATH_XML : {MD_Configuration.TOOL_PATH_XML.ToString()}");                
                log.Info($"Path Log Error : {MD_Configuration.TOOL_LOG_ERROR.ToString()}");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Effettuo la verifica se le cartelle Principali sono state Create
        /// </summary>
        static public void VerifyPrincipalFolder()
        {
            try
            {                
                if (!File.Exists(MD_Configuration.TOOL_PATH_XML)) { throw new IOException($"XML di configurazione per la query XML non trovato al path {MD_Configuration.TOOL_PATH_XML}"); }
                if (!Directory.Exists(MD_Configuration.TOOL_LOG_ERROR)) { Directory.CreateDirectory(MD_Configuration.TOOL_LOG_ERROR); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        static public void VerifyPrincipalFolder(NESPRESSO_Project _CRIF_Project)
        {
            try
            {
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_Input_SFTP_BACKUP) && !Directory.Exists(_CRIF_Project.NESPRESSO_Input_SFTP_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_Input_SFTP_BACKUP); }                
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_Input_WORK) && !Directory.Exists(_CRIF_Project.NESPRESSO_Input_WORK)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_Input_WORK); }
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP) && !Directory.Exists(_CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP); }                
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_LOG_ERROR) && !Directory.Exists(_CRIF_Project.NESPRESSO_LOG_ERROR)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_LOG_ERROR); }
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_LOG_ERROR) && !Directory.Exists(_CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE); }
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_LOG_ERROR) && !Directory.Exists(_CRIF_Project.NESPRESSO_BACKUP_CONSERVAZIONE)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_BACKUP_CONSERVAZIONE); }

                _CRIF_Project.NESPRESSO_Input_SFTP_BACKUP = Path.Combine(_CRIF_Project.NESPRESSO_Input_SFTP_BACKUP, _data);
                _CRIF_Project.NESPRESSO_Input_WORK = Path.Combine(_CRIF_Project.NESPRESSO_Input_WORK, _data);
                _CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP = Path.Combine(_CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP, _data);
                _CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE = Path.Combine(_CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE, _data);
                _CRIF_Project.NESPRESSO_BACKUP_CONSERVAZIONE = Path.Combine(_CRIF_Project.NESPRESSO_BACKUP_CONSERVAZIONE, _data);

                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_Input_SFTP_BACKUP) && !Directory.Exists(_CRIF_Project.NESPRESSO_Input_SFTP_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_Input_SFTP_BACKUP); }
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_Input_WORK) && !Directory.Exists(_CRIF_Project.NESPRESSO_Input_WORK)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_Input_WORK); }
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP) && !Directory.Exists(_CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_INPUT_WORK_BACKUP); }
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE) && !Directory.Exists(_CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE); }
                if (!string.IsNullOrEmpty(_CRIF_Project.NESPRESSO_OUTPUT_CONSERVAZIONE) && !Directory.Exists(_CRIF_Project.NESPRESSO_BACKUP_CONSERVAZIONE)) { Directory.CreateDirectory(_CRIF_Project.NESPRESSO_BACKUP_CONSERVAZIONE); }

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
