﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using NESPRESSO_NOTESPESE.XMLConfigurazione;
using MD.Configuration;

namespace NESPRESSO_NOTESPESE.XMLSerialize
{
    class NESPRESSO_XMLSerialize
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        private string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }

        public NESPRESSO SerializeXMLToNESPRESSO_Project()
        {
            NESPRESSO _oNESPRESSO = null;
            string path = string.Empty;
            try
            {
                path = Path.Combine(Environment.CurrentDirectory, MD_Configuration.TOOL_PATH_XML);
                log.Info($"Path XML : {path}");
                if (new FileInfo(path).Exists)
                {
                    XmlSerializer ser = new XmlSerializer(typeof(NESPRESSO));

                    using (Stream reader = new FileStream(path, FileMode.Open))
                    {
                        // Call the Deserialize method to restore the object's state.
                        _oNESPRESSO = (NESPRESSO)ser.Deserialize(reader);
                    }
                }
                else
                {
                    throw new Exception($"File di configurazione non trovato { path } ");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _oNESPRESSO;
        }
    }
}
