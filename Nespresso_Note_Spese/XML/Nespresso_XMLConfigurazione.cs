﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NESPRESSO_NOTESPESE.XMLConfigurazione
{
    [XmlRoot(ElementName = "NESPRESSO_MAIL")]
    public class NESPRESSO_MAIL
    {
        [XmlElement(ElementName = "NESPRESSO_Project_sendemail")]
        public System.Boolean NESPRESSO_Project_sendemail { get; set; }
        [XmlElement(ElementName = "NESPRESSO_Project_smtpserver")]
        public string NESPRESSO_Project_smtpserver { get; set; }
        [XmlElement(ElementName = "NESPRESSO_Project_from")]
        public string NESPRESSO_Project_from { get; set; }
        [XmlElement(ElementName = "NESPRESSO_Project_to")]
        public string NESPRESSO_Project_to { get; set; }
        [XmlElement(ElementName = "NESPRESSO_Project_cc")]
        public string NESPRESSO_Project_cc { get; set; }
    }
    [XmlRoot(ElementName = "NESPRESSO_Project")]
    public class NESPRESSO_Project
    {
        [XmlElement(ElementName = "NESPRESSO_Tipo_Esecuzione")]
        public string NESPRESSO_Tipo_Esecuzione { get; set; }


        [XmlElement(ElementName = "NESPRESSO_Tipo_Esecuzione_Enable")]        
        public Boolean NESPRESSO_CERTIFICATO_PGP_Enable { get; set; }


        [XmlElement(ElementName = "NESPRESSO_Input_SFTP")]
        public string NESPRESSO_Input_SFTP { get; set; }
        
        [XmlElement(ElementName = "NESPRESSO_Input_SFTP_BACKUP_ENABLE")]
        public Boolean NESPRESSO_Input_SFTP_BACKUP_ENABLE { get; set; }

        [XmlElement(ElementName = "NESPRESSO_Input_SFTP_BACKUP")]
        public string NESPRESSO_Input_SFTP_BACKUP { get; set; }


        [XmlElement(ElementName = "NESPRESSO_Input_WORK")]
        public string NESPRESSO_Input_WORK { get; set; }

        [XmlElement(ElementName = "NESPRESSO_Input_WORK_BACKUP")]        
        public string NESPRESSO_INPUT_WORK_BACKUP { get; set; }


        [XmlElement(ElementName = "NESPRESSO_Output_CONSERVAZIONE")]
        public string NESPRESSO_OUTPUT_CONSERVAZIONE { get; set; }

        [XmlElement(ElementName = "NESPRESSO_Backup_CONSERVAZIONE")]
        public string NESPRESSO_BACKUP_CONSERVAZIONE { get; set; }

        
        [XmlElement(ElementName = "NESPRESSO_LOG_ERROR")]
        public string NESPRESSO_LOG_ERROR { get; set; }


        [XmlElement(ElementName = "NESPRESSO_MAIL")]
        public NESPRESSO_MAIL NESPRESSO_MAIL { get; set; }
    }

    [XmlRoot(ElementName = "NESPRESSO")]
    public class NESPRESSO
    {
        [XmlElement(ElementName = "NESPRESSO_Project")]
        public List<NESPRESSO_Project> NESPRESSO_Project { get; set; }
    }

}
