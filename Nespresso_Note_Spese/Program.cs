﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NESPRESSO_NOTESPESE.XMLSerialize;
using NESPRESSO_NOTESPESE.XMLConfigurazione;
using NESPRESSO_NOTESPESE;
using MD.Errore;
using MD.Mail;
using System.IO;
using MD.Zip;
using MD.Configuration;
using MD.Elaborazione;

namespace Nespresso_Note_Spese
{
    class Program : MD_Errori
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static NESPRESSO_XMLSerialize NESPRESSO_XMLSerialize = new NESPRESSO_XMLSerialize();
        static void Main(string[] args)
        {
            log.Info("Begin - NESPRESSO NOTE SPESE");
            try
            {

                Nespresso_note_spese();

            }
            catch (Exception ex)
            {
                log.Error(ex, ex.Message);
                if (ex.InnerException != null) { log.Error(ex, ex.InnerException.StackTrace); }
                
                Errore_List.Add(ex.Message);
                log.Error(ex, ex.StackTrace);
            }
            finally
            {
                //verifico se vi sono degli errori gestiti all'interno dell'applicazione
                if (Errore_List.Count > 0)
                {
                    MD_Mail _Nespresso_Mail = new MD_Mail();
                    System.String _log_File_Error = String.Empty;
                    _log_File_Error = _Nespresso_Mail.CreateLogfile(Errore_List);
                    log.Info(_log_File_Error);
                    List<System.String> _errorFile = null;
                    if (File.Exists(_log_File_Error)) { _errorFile = new List<string> { _log_File_Error }; }
                    SendMail(_errorFile);
                    log.Info($"Cancellazione file di errore nella cartella dei Log");
                }
                log.Info("End - NESPRESSO NOTE SPESE");
                createMailLogFile();
            }
        }

        private static void Nespresso_note_spese()
        {
            try
            {

                NESPRESSO oNESPRESSO = NESPRESSO_XMLSerialize.SerializeXMLToNESPRESSO_Project();
                if (oNESPRESSO != null)
                {
                    log.Info("Oggetto XML Configurazione Caricato Correttamente");
                    foreach (NESPRESSO_Project _oNespressoProject in oNESPRESSO.NESPRESSO_Project)
                    {
                        //verifico la presenza delle relative cartelle
                        MD_Configuration.VerifyPrincipalFolder();
                        MD_Configuration.VerifyPrincipalFolder(_oNespressoProject);

                        //Effettuo Il backup dei relativi Files Arrivati sull'SFTP
                        Backup_Folder(_oNespressoProject);

                        //effettuo la copia all'interno della cartella di lavoro
                        Copy_ToWorkDirectory(_oNespressoProject);

                        MD_Elaborazione oMD_Elaborazione = new MD_Elaborazione();
                        oMD_Elaborazione.ElaboraProcedura(_oNespressoProject);
                    }
                }
                else
                {
                    log.Error("Oggetto XML non correttamente configurato");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static void Copy_ToWorkDirectory(NESPRESSO_Project _oNespressoProject)
        {
            try
            {
                DirectoryInfo odirectoryInfo = new DirectoryInfo(_oNespressoProject.NESPRESSO_Input_SFTP);
                List<FileInfo> oListFileInfo = odirectoryInfo.GetFiles("*", SearchOption.AllDirectories).ToList();

                if (oListFileInfo != null && oListFileInfo.Count > 0)
                {
                    foreach (FileInfo oFile in oListFileInfo)
                    {
                        if (odirectoryInfo.Name.Equals(new DirectoryInfo(oFile.DirectoryName).Name))
                        {
                            File.Copy(oFile.FullName, Path.Combine(_oNespressoProject.NESPRESSO_Input_WORK, oFile.Name));
                        }
                        else
                        {
                            if (!Directory.Exists(Path.Combine(_oNespressoProject.NESPRESSO_Input_WORK, oFile.Directory.Name))) { Directory.CreateDirectory(Path.Combine(_oNespressoProject.NESPRESSO_Input_WORK, oFile.Directory.Name)); }

                            File.Copy(oFile.FullName, Path.Combine(_oNespressoProject.NESPRESSO_Input_WORK, oFile.Directory.Name, oFile.Name));
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static void Backup_Folder(NESPRESSO_Project _oNespressoProject)
        {
            try
            {
                if (_oNespressoProject.NESPRESSO_Input_SFTP_BACKUP_ENABLE)
                {
                    try
                    {
                        MD_Zip oMD_Zip = new MD_Zip();
                        oMD_Zip.Zip_Directory(@_oNespressoProject.NESPRESSO_Input_SFTP, @_oNespressoProject.NESPRESSO_Input_SFTP_BACKUP);
                    }
                    catch (Exception)
                    {
                        Errore_List.Add("Backup Document SFTP Error - Problema nel backup dei documenti depositati sull'SFTP");
                        throw;
                    }
                }
                else {
                    log.Info("BACKUP disabilitato");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        static void createMailLogFile()
        {
            System.String _cartellaLog = System.DateTime.Now.ToString("yyyy-MM-dd");
            System.String _pathCartella = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log", _cartellaLog);
            List<System.String> _listFile = new List<string> { };
            try
            {
                log.Info($"Cartella Dei Log {_pathCartella}");
                NLog.LogManager.Shutdown();
                if (Directory.GetFiles(_pathCartella).Length > 0)
                {
                    _listFile = Directory.GetFiles(_pathCartella, "*", SearchOption.TopDirectoryOnly).ToList();
                }
                InviaMailLog(_listFile);
            }
            catch (Exception)
            {
                throw;
            }

        }

        private static void InviaMailLog(List<System.String> _LogFile)
        {
            MD_Mail _Nespresso_Mail = new MD_Mail();
            try
            {
                _Nespresso_Mail.email_from = System.Configuration.ConfigurationManager.AppSettings["MailFrom_Log"].ToString();
                _Nespresso_Mail.email_to = System.Configuration.ConfigurationManager.AppSettings["MailTo_Log"].ToString();
                _Nespresso_Mail.email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Abilitazione_Mail_Log"].ToString());
                _Nespresso_Mail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["MailSmtp_Log"].ToString();
                _Nespresso_Mail.email_Attachement = _LogFile;

                _Nespresso_Mail.SendMailLog();

            }
            catch (Exception)
            {
                throw;
            }
        }

        private static void SendMail(List<System.String> _errorFile)
        {
            MD_Mail _Nespresso_Mail = new MD_Mail();
            Boolean _invioMailEccezione = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_eccezione"].ToString());
            try
            {
                log.Info("Start => SendMail");
                _Nespresso_Mail.email_from = System.Configuration.ConfigurationManager.AppSettings["email_from"].ToString();
                _Nespresso_Mail.email_to = System.Configuration.ConfigurationManager.AppSettings["email_to"].ToString();
                _Nespresso_Mail.email_cc = System.Configuration.ConfigurationManager.AppSettings["email_cc"].ToString();
                _Nespresso_Mail.email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_sendemail"].ToString());
                _Nespresso_Mail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["email_smtpserver"].ToString();               
                _Nespresso_Mail.email_Attachement = _errorFile;
                //invio mail a Microframe
                _Nespresso_Mail.SendMailError(true);
                
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
